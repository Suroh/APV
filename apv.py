import sys
import os
import pandas as pd
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QPushButton, QDialog, QLabel, QRadioButton, QAction, QLineEdit, QTableWidget, QDockWidget, QTextEdit, QTableWidgetItem, QVBoxLayout, QActionGroup, QButtonGroup, QComboBox
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot, QUrl, QEvent
from PyQt5.QtGui import QFont
import xlrd
import openpyxl
from IPython.display import IFrame
import pyexcel as p
import json
import datetime
import pyexcel
import sqlite3
import csv

class Installer(object):
    '''
    '''
    def __init__(self):
        '''
        '''
        self.windows_folders = ['C:\\APV\\',
                                'C:\\APV\\Excel\\',
                                'C:\\APV\\Csv\\',
                                'C:\\APV\\Html\\',
                                'C:\\APV\\Text\\',
                                'C:\\APV\\User Manual\\',
                                'C:\\APV\\Documentation',
                                'C:\\APV\\Images\\',
                                'C:\\APV\\Sqllite\\']

        self.windows_files = ['C:\\APV\\Excel\\Private.xlsx',
                              'C:\\APV\\Csv\\Private.csv',
                              'C:\\APV\\Html\\Private.html',
                              'C:\\APV\\Text\\Private.txt',
                              'C:\\APV\\Sqllite\Private.db']

        self.linux_folders = ['/home/' + str(os.getlogin()) + '/.APV/',
                              '/home/' + str(os.getlogin()) + '/.APV/Excel/',
                              '/home/' + str(os.getlogin()) + '/.APV/Csv/',
                              '/home/' + str(os.getlogin()) + '/.APV/Html/',
                              '/home/' + str(os.getlogin()) + '/.APV/Text/',
                              '/home/' + str(os.getlogin()) + '/.APV/User Manual/',
                              '/home/' + str(os.getlogin()) + '/.APV/Documentation/',
                              '/home/' + str(os.getlogin()) + '/.APV/Images/',
                              '/home/' + str(os.getlogin()) + '/.APV/Sqllite/']

        self.linux_files = ['/home/' + str(os.getlogin()) + '/.APV/Excel/Private.xlsx',
                            '/home/' + str(os.getlogin()) + '/.APV/Html/Private.html',
                            '/home/' + str(os.getlogin()) + '/.APV/Text/Private.txt',
                            '/home/' + str(os.getlogin()) + '/.APV/Csv/Private.csv',
                            '/home/' + str(os.getlogin()) + '/.APV/Sqllite/Private.db']

    def display_directories(self):
        '''
        '''
        print('\nPrinting Windows folders:')
        for i in self.windows_folders:
            print(i)

        print('\nPrinting Windows files:')
        for i in self.windows_files:
            print(i)

        print('\nPrinting Linux folders:')
        for i in self.linux_folders:
            print(i)

        print('\nPrinting Linux files:')
        for i in self.linux_files:
            print(i)

    def create_directory(self, path):
        '''
        '''
        os.mkdir(str(path))
        print('Created: ' + str(path) + ' on: ' + self.get_time())

    def create_file(self, path):
        '''
        '''
        try:
            if sys.platform.startswith('linux'):
                if path == self.linux_files[0]: #'/home/' + str(os.getlogin()) + '/.APV/Excel/Private.xlsx'
                    self.create_excel_document(path)
                    print('Created: ' + str(path) + ' on: ' + self.get_time())
                elif path == self.linux_files[4]:
                    database = '/home/' + str(os.getlogin()) + '/.APV/Sqllite/Private.db'
                    connection = sqlite3.connect(database)
                    c = connection.cursor()
                    c.execute("CREATE TABLE IF NOT EXISTS Accounts(Listing text PRIMARY KEY,"
                                  "Website text,"
                                  "Email text,"
                                  "Username text,"
                                  "Password text)")
                else:
                    with open(str(path), 'w') as outFile:
                        pass
                    print('Created: ' + str(path) + ' on: ' + self.get_time())
            elif sys.platform.startswith('win'):
                if path == 'C:\\APV\\Excel\\Private.xlsx':
                    self.create_excel_document(path)
                    print('Created: ' + str(path) + ' on: ' + self.get_time())
                elif path == 'C:\\APV\\Sqllite\Private.db':
                    database = 'C:\\APV\\Sqllite\\Private.db'
                    connection = sqlite3.connect(database)
                    c = connection.cursor()
                    c.execute("CREATE TABLE IF NOT EXISTS Accounts(Listing text PRIMARY KEY,"
                                  "Website text,"
                                  "Email text,"
                                  "Username text,"
                                  "Password text)")
                else:
                    with open(str(path), 'w') as outFile:
                        pass
                    print('Created: ' + str(path) + ' on: ' + self.get_time())
        except Exception as e:
            pass

    def check_directories(self):
        '''
        '''
        try:
            if sys.platform.startswith('linux'):
                for i in self.linux_folders:
                    if os.path.isdir(str(i)):
                        pass
                    elif not os.path.isdir(str(i)):
                        self.create_directory(str(i))
            elif sys.platform.startswith('win'):
                for i in self.windows_folders:
                    if os.path.isdir(str(i)):
                        pass
                    elif not os.path.isdir(str(i)):
                        self.create_directory(str(i))
        except OSError as a:
            print('OSError: ' + str(a))
        except IOError as b:
            print('IOError: ' + str(b))

    def check_files(self):
        '''
        '''
        try:
            if sys.platform.startswith('linux'):
                for i in self.linux_files:
                    if os.path.isfile(str(i)):
                        pass
                    elif not os.path.isfile(str(i)):
                        self.create_file(str(i))
            elif sys.platform.startswith('win'):
                for i in self.windows_files:
                    if os.path.isfile(str(i)):
                        pass
                    elif not os.path.isfile(str(i)):
                        self.create_file(str(i))
        except OSError as a:
            print('OSError: ' + str(a))
        except IOError as b:
            print('IOError: ' + str(b))

    def integrity_check(self):
        '''
        '''
        self.check_directories()
        self.check_files()

    def get_time(self):
        '''
        '''
        return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    def create_excel_document(self, path):
        try:
            if sys.platform.startswith('linux'):
                if os.path.isdir(path):
                    if os.path.isfile(path + 'Private.xlsx'): # .ods for linux so this should be changed
                        pass
                    elif not os.path.isfile(path):
                        df1 = pd.DataFrame({'Listing': ['Example Listing'], 'Website': ['www.examplewesbite.com'],
                                            'Email': ['ExampleEmail@email.com'], 'Username': ['ExampleUserName'],
                                            'Password': ['ExamplePassword1234']})
                        writer = pd.ExcelWriter(path, engine='xlsxwriter')
                        df1.to_excel(writer, sheet_name='Accounts')
                        workbook = writer.book
                        worksheet1 = writer.sheets['Accounts']
                        worksheet1.set_column(1, 5, 35) #
                        writer.save()
                elif not os.path.isdir(path):
                    df1 = pd.DataFrame({'Listing': ['Example Listing'], 'Website': ['www.examplewesbite.com'],
                                        'Email': ['ExampleEmail@email.com'], 'Username': ['ExampleUserName'],
                                        'Password': ['ExamplePassword1234']})
                    writer = pd.ExcelWriter(path, engine='xlsxwriter')
                    df1.to_excel(writer, sheet_name='Accounts')
                    workbook = writer.book
                    worksheet1 = writer.sheets['Accounts']
                    worksheet1.set_column(1, 5, 35) #
                    writer.save()
            elif sys.platform.startswith('win32'):
                if os.path.isdir(path):
                    if os.path.isfile(path + 'Private.xlsx'):
                        pass
                    elif not os.path.isfile(path):
                        df1 = pd.DataFrame({'Listing': ['Example Listing'], 'Website': ['www.examplewesbite.com'],
                                            'Email': ['ExampleEmail@email.com'], 'Username': ['ExampleUserName'],
                                            'Password': ['ExamplePassword1234']})
                        writer = pd.ExcelWriter(path, engine='xlsxwriter')
                        df1.to_excel(writer, sheet_name='Accounts')
                        workbook = writer.book
                        worksheet1 = writer.sheets['Accounts']
                        worksheet1.set_column(1, 5, 35)
                        writer.save()
                elif not os.path.isdir(path):
                    df1 = pd.DataFrame({'Listing': ['Example Listing'], 'Website': ['www.examplewesbite.com'],
                                        'Email': ['ExampleEmail@email.com'], 'Username': ['ExampleUserName'],
                                        'Password': ['ExamplePassword1234']})
                    writer = pd.ExcelWriter(path, engine='xlsxwriter')
                    df1.to_excel(writer, sheet_name='Accounts')
                    workbook = writer.book
                    worksheet1 = writer.sheets['Accounts']
                    worksheet1.set_column(1, 5, 35)
                    writer.save()
            elif sys.platform == 'darwin':
                if os.path.isdir(path):
                    if os.path.isfile(path + 'Private.xlsx'): # .ods for mac so this should be changed
                        pass
                    elif not os.path.isfile(path):
                        df1 = pd.DataFrame({'Listing': ['Example Listing'], 'Website': ['www.examplewesbite.com'],
                                            'Email': ['ExampleEmail@email.com'], 'Username': ['ExampleUserName'],
                                            'Password': ['ExamplePassword1234']})
                        writer = pd.ExcelWriter(path, engine='xlsxwriter')
                        df1.to_excel(writer, sheet_name='Accounts')
                        workbook = writer.book
                        worksheet1 = writer.sheets['Accounts']
                        worksheet1.set_column(1, 5, 35)
                        writer.save()
                elif not os.path.isdir(path):
                    df1 = pd.DataFrame({'Listing': ['Example Listing'], 'Website': ['www.examplewesbite.com'],
                                        'Email': ['ExampleEmail@email.com'], 'Username': ['ExampleUserName'],
                                        'Password': ['ExamplePassword1234']})
                    writer = pd.ExcelWriter(path, engine='xlsxwriter')
                    df1.to_excel(writer, sheet_name='Accounts')
                    workbook = writer.book
                    worksheet1 = writer.sheets['Accounts']
                    worksheet1.set_column(1, 5, 35)
                    writer.save()
        except IOError as exception:
            raise IOError('%s: %s' % (exception.strerror))
        return None


class WarningDialog(QDialog):
    '''
    '''
    def __init__(self, message, parent = None):
        super(WarningDialog, self).__init__(parent)
        self.message = message
        self.popup = QLabel(self.message, self)


class Apv(QMainWindow):

    def __init__(self):
        '''
        '''
        super().__init__()
        self.title = 'Apv Alpha .5'
        self.left = 10
        self.top = 10
        self.width = 800
        self.height = 650

        self.windows_folders = ['C:\\APV\\',
                            'C:\\APV\\Excel\\',
                            'C:\\APV\\Csv\\',
                            'C:\\APV\\Html\\',
                            'C:\\APV\\Text\\',
                            'C:\\APV\\User Manual\\',
                            'C:\\APV\\Documentation',
                            'C:\\APV\\Images\\',
                            'C:\\APV\\Sqllite\\']

        self.windows_files = ['C:\\APV\\Excel\\Private.xlsx',
                          'C:\\APV\\Csv\\Private.csv',
                          'C:\\APV\\Html\\Private.html',
                          'C:\\APV\\Text\\Private.txt',
                          'C:\\APV\\Sqllite\Private.db']

        self.linux_folders = ['/home/' + str(os.getlogin()) + '/.APV/',
                          '/home/' + str(os.getlogin()) + '/.APV/Excel/',
                          '/home/' + str(os.getlogin()) + '/.APV/Csv/',
                          '/home/' + str(os.getlogin()) + '/.APV/Html/',
                          '/home/' + str(os.getlogin()) + '/.APV/Text/',
                          '/home/' + str(os.getlogin()) + '/.APV/User Manual/',
                          '/home/' + str(os.getlogin()) + '/.APV/Documentation/',
                          '/home/' + str(os.getlogin()) + '/.APV/Images/',
                          '/home/' + str(os.getlogin()) + '/.APV/Sqllite/']

        self.linux_files = ['/home/' + str(os.getlogin()) + '/.APV/Excel/Private.xlsx',
                        '/home/' + str(os.getlogin()) + '/.APV/Html/Private.html',
                        '/home/' + str(os.getlogin()) + '/.APV/Text/Private.txt',
                        '/home/' + str(os.getlogin()) + '/.APV/Csv/Private.csv',
                        '/home/' + str(os.getlogin()) + '/.APV/Sqllite/Private.db']

        self.__master_dict = {'Listing': None, 'Website': None, 'Email': None, 'Username': None, 'Password': None}
        Installer().integrity_check()
        self.__init__ui()

    def __init__ui(self):
        '''
        '''
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.listing_gui = QLineEdit('', self)
        self.listing_gui.setFont(QFont('Arial', 12))
        self.listing_gui.setPlaceholderText('Listing: ')
        self.listing_gui.resize(200, 30)
        self.listing_gui.move(0, 30)
        self.listing_gui.returnPressed.connect(self.get_listing)
        #width = listing_gui.frameGeometry().width()
        #height = listing_gui.frameGeometry().height()
        #print(width)
        #print(height)
        self.website_gui = QLineEdit('', self)
        self.website_gui.setFont(QFont('Arial', 12))
        self.website_gui.setPlaceholderText('Website: ')
        self.website_gui.resize(200, 30)
        self.website_gui.move(0, 60)
        self.website_gui.returnPressed.connect(self.get_website)
        self.email_gui = QLineEdit('', self)
        self.email_gui.setFont(QFont('Arial', 12))
        self.email_gui.setPlaceholderText('Email Address: ')
        self.email_gui.resize(200, 30)
        self.email_gui.move(0, 90)
        self.email_gui.returnPressed.connect(self.get_email)
        self.username_gui = QLineEdit('', self)
        self.username_gui.setFont(QFont('Arial', 12))
        self.username_gui.setPlaceholderText('Username: ')
        self.username_gui.resize(200, 30)
        self.username_gui.move(0, 120)
        self.username_gui.returnPressed.connect(self.get_username)
        self.password_gui = QLineEdit('', self)
        self.password_gui.setFont(QFont('Arial', 12))
        self.password_gui.setPlaceholderText('Password: ')
        self.password_gui.resize(200, 30)
        self.password_gui.move(0, 150)
        self.password_gui.returnPressed.connect(self.get_password)
        self.table = QTableWidget(self)
        self.table.setColumnCount(5)
        self.table.setRowCount(1)
        self.table.setHorizontalHeaderLabels('Listing;Website;Email Address;Username;Password'.split(";"))
        self.table.setItem(0, 0, QTableWidgetItem('Example Listing'))
        self.table.setItem(0, 1, QTableWidgetItem('https://example_website.com'))
        self.table.setItem(0, 2, QTableWidgetItem('ExampleEmail@email.com'))
        self.table.setItem(0, 3, QTableWidgetItem('ExampleUser66'))
        self.table.setItem(0, 4, QTableWidgetItem('ExAmPlEpassW0rd872@##@1'))
        self.table.resizeColumnsToContents()
        self.table.resizeRowsToContents()
        dock = QDockWidget('Table', self)
        dock.setWidget(self.table)
        dock.resize(800, 650 - 200)
        dock.move(0, 180)
        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('File')
        exitButton = QAction('Exit', self)
        exitButton.setShortcut('Ctrl+Q')
        exitButton.setStatusTip('Exit application')
        exitButton.triggered.connect(self.close)
        printButton = QAction('Print File', self)
        printButton.setShortcut('Ctrl+P')
        printButton.setStatusTip('Print File')
        #printButton.triggered.connect()
        fileMenu.addAction(printButton)
        fileMenu.addAction(exitButton)
        editMenu = mainMenu.addMenu('Edit')
        viewMenu = mainMenu.addMenu('View')
        displayButton = QAction('Display All', self)
        displayButton.setShortcut('Ctrl+V')
        displayButton.setStatusTip('Display Entire File')
        displayButton.triggered.connect(self.display_database)
        viewMenu.addAction(displayButton)
        searchMenu = mainMenu.addMenu('Search')
        searchStringButton = QAction('Search String', self)
        searchStringButton.setShortcut('Ctrl+S')
        searchStringButton.setStatusTip('Enter listing name in text box to search for that listing')
        #searchStringButton.triggered.connect()
        searchRowButton = QAction('Search Row', self)
        searchRowButton.setShortcut('Ctrl+R')
        searchRowButton.setStatusTip('Search a specific row')
        #searchRowButton.triggered.connect()
        searchColumnButton = QAction('Search Column', self)
        searchColumnButton.setShortcut('Ctrl+Y')
        searchColumnButton.setStatusTip('Enter the specific column you wish to search')
        #searchColumnButton.triggered.connect()
        searchMenu.addAction(searchStringButton)
        searchMenu.addAction(searchRowButton)
        searchMenu.addAction(searchColumnButton)
        settingsMenu = mainMenu.addMenu('Settings')
        fileTypeMenu = settingsMenu.addMenu('File Type')
        self.excelButton = QAction('Excel', self, checkable = True)
        self.excelButton.setStatusTip('Writes data to an excel spreadsheet')
        self.excelButton.triggered.connect(self.__is_excel_checked)
        self.sqlButton = QAction('Sqllite', self, checkable = True)
        self.sqlButton.setStatusTip('Writes data to an sqllite3 database')
        self.sqlButton.triggered.connect(self.__is_sql_checked)
        self.textButton = QAction('Text', self, checkable = True)
        self.textButton.setStatusTip('Writes data to a text file')
        self.textButton.triggered.connect(self.__is_text_checked)
        self.htmlButton = QAction('Html', self, checkable = True)
        self.htmlButton.setStatusTip('Writes data to an html file')
        self.htmlButton.triggered.connect(self.__is_html_checked)
        self.csvButton = QAction('Csv', self, checkable = True)
        self.csvButton.setStatusTip('Writes data to a csv file')
        self.csvButton.triggered.connect(self.__is_csv_checked)
        fileTypeMenu.addAction(self.excelButton)
        fileTypeMenu.addAction(self.sqlButton)
        fileTypeMenu.addAction(self.textButton)
        fileTypeMenu.addAction(self.htmlButton)
        fileTypeMenu.addAction(self.csvButton)
        helpMenu = mainMenu.addMenu('Help')
        self.show()

    def get_listing(self):
        '''
        '''
        __listing = self.listing_gui.text()
        self.__master_dict['Listing'] = __listing
        self.listing_gui.setText('')

    def get_website(self):
        '''
        '''
        __website = self.website_gui.text()
        self.__master_dict['Website'] = __website
        self.website_gui.setText('')

    def get_email(self):
        '''
        '''
        __email = self.email_gui.text()
        self.__master_dict['Email'] = __email
        self.email_gui.setText('')

    def get_username(self):
        '''
        '''
        __usr = self.username_gui.text()
        self.__master_dict['Username'] = __usr
        self.username_gui.setText('')

    def get_password(self):
        '''
        '''
        if sys.platform.startswith('linux'):
            __password = self.password_gui.text()
            self.__master_dict['Password'] = __password
            if self.excelButton.isChecked():
                self.append_excel(str(self.linux_files[0]))
            elif self.sqlButton.isChecked():
                self.append_sql()
            elif self.csvButton.isChecked():
                self.append_csv(self.linux_files[3])
            elif self.htmlButton.isChecked():
                print('Must implement')
            elif self.textButton.isChecked():
                print('Must implement')
            else:
                s = ('You need to check at least one file type. \nPlease'
                    ' go to the settings menu and select a file to work'
                    ' with. \nWriting to excel by default.')
                test = WarningDialog(s, self)
                test.setGeometry(100, 50, 400, 100)
                test.show()
                self.excelButton.setChecked(True)
                self.append_excel(str(self.linux_files[0]))
            self.password_gui.setText('')
        elif sys.platform.startswith('win'):
            __password = self.password_gui.text()
            self.__master_dict['Password'] = __password
            if self.excelButton.isChecked():
                self.append_excel(str(self.windows_files[0]))
            elif self.sqlButton.isChecked():
                self.append_sql()
            elif self.csvButton.isChecked():
                self.append_csv(self.windows_files[1])
            elif self.htmlButton.isChecked():
                print('Must implement')
            elif self.textButton.isChecked():
                print('Must implement')
            else:
                s = ('You need to check at least one file type. \nPlease'
                    ' go to the settings menu and select a file to work'
                    ' with. \nWriting to excel file by default')
                test = WarningDialog(s, self)
                test.setGeometry(100, 50, 400, 100)
                test.show()
                self.excelButton.setChecked(True)
                self.append_excel(str(self.windows_files[0]))
            self.password_gui.setText('')

    def append_excel(self, path):
        '''
        '''
        try:
            if sys.platform.startswith('linux'):
                df = pd.read_excel(path, sheet_name='Accounts')
                df1 = pd.DataFrame({'Listing': [self.__master_dict['Listing']], 'Website': [self.__master_dict['Website']],
                                    'Email': [self.__master_dict['Email']], 'Username': [self.__master_dict['Username']],
                                    'Password': [self.__master_dict['Password']]})
                df3 = df.append(df1)
                df3 = df3[['Listing', 'Website', 'Email', 'Username', 'Password']]
                writer = pd.ExcelWriter(path, engine='xlsxwriter')
                df3.to_excel(writer, sheet_name='Accounts')
                worksheet1 = writer.sheets['Accounts']
                worksheet1.set_column(1, 5, 35)
                writer.save()
                self.__master_dict.clear()
            elif sys.platform.startswith('win'):
                df = pd.read_excel(path, sheet_name='Accounts')
                df1 = pd.DataFrame({'Listing': [self.__master_dict['Listing']], 'Website': [self.__master_dict['Website']],
                                    'Email': [self.__master_dict['Email']], 'Username': [self.__master_dict['Username']],
                                    'Password': [self.__master_dict['Password']]})
                df3 = df.append(df1)
                df3 = df3[['Listing', 'Website', 'Email', 'Username', 'Password']]
                writer = pd.ExcelWriter(path, engine='xlsxwriter')
                df3.to_excel(writer, sheet_name='Accounts')
                worksheet1 = writer.sheets['Accounts']
                worksheet1.set_column(1, 5, 35)
                writer.save()
                self.__master_dict.clear()
        except IOError as a:
            pass
        except FileNotFoundError as b:
            pass

    def append_html(self, path):
        pass

    def append_sql(self):
        try:
            if sys.platform.startswith('linux'):
                database = self.linux_files[4]
                connection = sqlite3.connect(database)
                c = connection.cursor()
                c.execute("INSERT INTO Accounts VALUES(?, ?, ?, ?, ?)", [self.__master_dict['Listing'], self.__master_dict['Website'], self.__master_dict['Email'], self.__master_dict['Username'], self.__master_dict['Password']])
                connection.commit()
                connection.close()
            elif sys.platform.startswith('win'):
                database = self.windows_files[4]
                connection = sqlite3.connect(database)
                c = connection.cursor()
                c.execute("INSERT INTO Accounts VALUES(?, ?, ?, ?, ?)", [self.__master_dict['Listing'], self.__master_dict['Website'], self.__master_dict['Email'], self.__master_dict['Username'], self.__master_dict['Password']])
                connection.commit()
                connection.close()
        except FileNotFoundError as a:
            pass

    def append_csv(self, path):
        with open(path, 'a') as Out:
            writer = csv.DictWriter(Out, delimiter=' ',
            fieldnames=self.__master_dict, quotechar='|',
            quoting=csv.QUOTE_MINIMAL)

            writer.writeheader()
            writer.writerow({'Listing': self.__master_dict['Listing']})
            writer.writerow({'Website': self.__master_dict['Website']})
            writer.writerow({'Email': self.__master_dict['Email']})
            writer.writerow({'Username': self.__master_dict['Username']})
            writer.writerow({'Password': self.__master_dict['Password']})
    def append_text(self, path):
        pass

    def __read_excel(self, path):
        '''
        '''
        try:
            if sys.platform.startswith('linux'):
                df = pd.read_excel(path)
                self.table.setColumnCount(len(df.columns))
                self.table.setRowCount(len(df.index))
                for i in range(len(df.index)):
                    for j in range(len(df.columns)):
                        self.table.setItem(i, j, QTableWidgetItem(str(df.iat[i, j])))
                self.table.resizeColumnsToContents()
                self.table.resizeRowsToContents()
            elif sys.platform.startswith('win'):
                df = pd.read_excel(path)
                self.table.setColumnCount(len(df.columns))
                self.table.setRowCount(len(df.index))
                for i in range(len(df.index)):
                    for j in range(len(df.columns)):
                        self.table.setItem(i, j, QTableWidgetItem(str(df.iat[i, j])))
                self.table.resizeColumnsToContents()
                self.table.resizeRowsToContents()
            elif sys.platform == 'darwin':
                pass
        except IOError as a:
            print('IOError' + str(a))

    def __is_excel_checked(self):
        if sys.platform.startswith('linux'):
            '''
            s = ("Warning: Your operating system doesn't support excel.\nPlease"
                " choose another file type to work with in the settings menu"
                "\nSetting file type to .db by default.")
            test = WarningDialog(s, self)
            test.setGeometry(100, 50, 400, 100)
            test.show()
            '''
            # self.excelButton.setChecked(False)
            self.sqlButton.setChecked(False)
            self.htmlButton.setChecked(False)
            self.csvButton.setChecked(False)
            self.textButton.setChecked(False)
        elif sys.platform.startswith('win'):
            self.sqlButton.setChecked(False)
            self.htmlButton.setChecked(False)
            self.csvButton.setChecked(False)
            self.textButton.setChecked(False)

    def __is_sql_checked(self):
        if sys.platform.startswith('linux'):
            self.excelButton.setChecked(False)
            self.htmlButton.setChecked(False)
            self.csvButton.setChecked(False)
            self.textButton.setChecked(False)
        elif sys.platform.startswith('win'):
            self.excelButton.setChecked(False)
            self.htmlButton.setChecked(False)
            self.csvButton.setChecked(False)
            self.textButton.setChecked(False)

    def __is_html_checked(self):
        if sys.platform.startswith('linux'):
            self.excelButton.setChecked(False)
            self.sqlButton.setChecked(False)
            self.csvButton.setChecked(False)
            self.textButton.setChecked(False)
        elif sys.platform.startswith('win'):
            self.excelButton.setChecked(False)
            self.sqlButton.setChecked(False)
            self.csvButton.setChecked(False)
            self.textButton.setChecked(False)

    def __is_csv_checked(self):
        if sys.platform.startswith('linux'):
            self.excelButton.setChecked(False)
            self.sqlButton.setChecked(False)
            self.htmlButton.setChecked(False)
            self.textButton.setChecked(False)
        elif sys.platform.startswith('win'):
            self.excelButton.setChecked(False)
            self.sqlButton.setChecked(False)
            self.htmlButton.setChecked(False)
            self.textButton.setChecked(False)

    def __is_text_checked(self):
        if sys.platform.startswith('linux'):
            self.excelButton.setChecked(False)
            self.sqlButton.setChecked(False)
            self.htmlButton.setChecked(False)
            self.csvButton.setChecked(False)
        elif sys.platform.startswith('win'):
            self.excelButton.setChecked(False)
            self.sqlButton.setChecked(False)
            self.htmlButton.setChecked(False)
            self.csvButton.setChecked(False)

    def display_database(self):
        '''
        '''
        try:
            if sys.platform.startswith('linux'):
                if self.excelButton.isChecked():
                    self.__read_excel(str(self.linux_files[0]))
                elif self.sqlButton.isChecked():
                    print('Must implement')
                elif self.csvButton.isChecked():
                    print('Must implement')
                elif self.htmlButton.isChecked():
                    print('Must implement')
                elif self.textButton.isChecked():
                    print('Must implement')
                else:
                    s = ('You need to check at least one file type. \nPlease'
                        ' go to the settings menu and select a file to work'
                        ' with.')
                    test = WarningDialog(s, self)
                    test.setGeometry(100, 50, 400, 100)
                    test.show()
            elif sys.platform.startswith('win'):
                self.__read_excel(str(self.windows_files[0]))
            elif sys.platform == 'darwin':
                pass
        except IOError as a:
            print('IOError: ' + str(a))


if __name__ == '__main__':
    apv = QApplication(sys.argv)
    ex = Apv()
    sys.exit(apv.exec_())
